#ifndef GEOMETRY_POLYGONS_CONVEX_H_
#define GEOMETRY_POLYGONS_CONVEX_H_

#include "geometry/polygons/polygon.pb.h"

namespace geometry {
namespace polygons {

double crossProduct(const Point &a, const Point &b, const Point &c);
bool is_convex(const Polygon &);

}  // namespace polygons
}  // namespace geometry

#endif
