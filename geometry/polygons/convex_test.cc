#include "geometry/polygons/convex.h"

#include "geometry/point.pb.h"
#include "geometry/polygons/polygon.pb.h"
#include "gtest/gtest.h"

namespace {

geometry::Point make_point(const double x, const double y) {
  geometry::Point point;
  point.set_x(x);
  point.set_y(y);
  return point;
}

// The fixture for testing class Foo.
class IsConvexTest : public ::testing::Test {};

// Tests that the Foo::Bar() method does Abc.
TEST_F(IsConvexTest, FailsOnEmpty) {
  geometry::polygons::Polygon polygon;
  ASSERT_EQ(polygon.vertices().size(), 0);
  EXPECT_DEATH(geometry::polygons::is_convex(polygon),
               "Polygon must have at least 3 vertices.");
}

TEST_F(IsConvexTest, AcceptsTriangle) {
  geometry::polygons::Polygon polygon;
  *polygon.add_vertices() = make_point(0.0, 0.0);
  *polygon.add_vertices() = make_point(10.0, 0.0);
  *polygon.add_vertices() = make_point(0.0, 10.0);
  EXPECT_TRUE(geometry::polygons::is_convex(polygon));
}

TEST_F(IsConvexTest, RejectsClockwiseTriangle) {
  geometry::polygons::Polygon polygon;
  *polygon.add_vertices() = make_point(0.0, 0.0);
  *polygon.add_vertices() = make_point(0.0, 10.0);
  *polygon.add_vertices() = make_point(10.0, 0.0);
  EXPECT_FALSE(geometry::polygons::is_convex(polygon));
}

TEST_F(IsConvexTest, AcceptsConvexQuadrilateral) {
  geometry::polygons::Polygon polygon;
  *polygon.add_vertices() = make_point(0.0, 0.0);
  *polygon.add_vertices() = make_point(10.0, 0.0);
  *polygon.add_vertices() = make_point(6.0, 6.0);
  *polygon.add_vertices() = make_point(0.0, 10.0);
  EXPECT_TRUE(geometry::polygons::is_convex(polygon));
}

TEST_F(IsConvexTest, RejectsConcaveQuadrilateral) {
  geometry::polygons::Polygon polygon;
  *polygon.add_vertices() = make_point(0.0, 0.0);
  *polygon.add_vertices() = make_point(10.0, 0.0);
  *polygon.add_vertices() = make_point(4.0, 4.0);
  *polygon.add_vertices() = make_point(0.0, 10.0);
  EXPECT_FALSE(geometry::polygons::is_convex(polygon));
}

}  // namespace

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
