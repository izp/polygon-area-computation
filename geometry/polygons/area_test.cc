#include "geometry/polygons/area.h"

#include "geometry/point.pb.h"
#include "geometry/polygons/polygon.pb.h"
#include "gtest/gtest.h"

namespace {

geometry::Point make_point(const double x, const double y) {
  geometry::Point point;
  point.set_x(x);
  point.set_y(y);
  return point;
}

// The fixture for testing class Foo.
class PolygonAreaTest : public ::testing::Test {};

TEST_F(PolygonAreaTest, TraingleArea) {
  geometry::polygons::Polygon polygon;
  *polygon.add_vertices() = make_point(0.0, 0.0);
  *polygon.add_vertices() = make_point(10.0, 0.0);
  *polygon.add_vertices() = make_point(0.0, 10.0);
  EXPECT_NEAR(geometry::polygons::polygonArea(polygon), 50.0, 1e-9);
}

TEST_F(PolygonAreaTest, NotConvexTriangle) {
  geometry::polygons::Polygon polygon;
  *polygon.add_vertices() = make_point(0.0, 0.0);
  *polygon.add_vertices() = make_point(0.0, 10.0);
  *polygon.add_vertices() = make_point(10.0, 0.0);
  EXPECT_DEATH(geometry::polygons::polygonArea(polygon),
               "Polygon must be convex!");
}

TEST_F(PolygonAreaTest, QuadrilateralArea) {
  geometry::polygons::Polygon polygon;
  *polygon.add_vertices() = make_point(0.0, 0.0);
  *polygon.add_vertices() = make_point(10.0, 0.0);
  *polygon.add_vertices() = make_point(10.0, 10.0);
  *polygon.add_vertices() = make_point(0.0, 10.0);
  EXPECT_NEAR(geometry::polygons::polygonArea(polygon), 100.0, 1e-9);
}

TEST_F(PolygonAreaTest, PentagonalArea) {
  geometry::polygons::Polygon polygon;
  *polygon.add_vertices() = make_point(0.0, 0.0);
  *polygon.add_vertices() = make_point(2.0, 0.0);
  *polygon.add_vertices() = make_point(4.0, 1.0);
  *polygon.add_vertices() = make_point(3.0, 4.0);
  *polygon.add_vertices() = make_point(0.0, 2.0);
  EXPECT_NEAR(geometry::polygons::polygonArea(polygon), 10.5, 1e-9);
}

}  // namespace

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
